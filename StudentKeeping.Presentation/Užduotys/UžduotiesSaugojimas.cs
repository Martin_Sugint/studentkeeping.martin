﻿using StudentKeeping.Presentation.Mokytojai;
using StudentKeeping.Presentation.Students;
using StudentKeeping.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentKeeping.Presentation.Užduotys
{
    public partial class UžduotiesSaugojimas : Form
    {
        private readonly UžduotysService _service;
        private readonly DalykaiService _dalykaiService;

        public UžduotiesSaugojimas()
        {
            InitializeComponent();
            _service = new UžduotysService();
            _dalykaiService = new DalykaiService();
            UžpildykDalykoDropdown();
            UžpildykFormą();
        }

        private void UžpildykFormą()
        {
            if (Session.Role == Dal.ViewModels.Role.Mokytojas)
            {
                txtMokytojoId.Text = Session.NaudotojoId.ToString();
                txtMokytojoVardasPavardė.Text = string.Format("{0} {1}", Session.Vardas, Session.Pavarde);
            }
        }


        private void UžpildykDalykoDropdown()
        {
            var dalykai = _dalykaiService.GaukDalykus();

            cmbxDalykoId.DataSource = new BindingSource(dalykai, null);
            cmbxDalykoId.DisplayMember = "Pavadinimas";
            cmbxDalykoId.ValueMember = "Id";
        }
        private void btnPickMokytojas_Click(object sender, EventArgs e)
        {
            MokytojųSąrašas mokytojųSąrašas = new MokytojųSąrašas();
            mokytojųSąrašas.FormClosed += MokytojųSąrašas_FormClosed;
            mokytojųSąrašas.ShowDialog();
        }

        private void MokytojųSąrašas_FormClosed(object sender, FormClosedEventArgs e)
        {
            IšvalykMokytojoLaukus();
            var forma = (MokytojųSąrašas)sender;
            var mokytojoId = forma.MokytojoId;
            var vardasPavardė = forma.MokytojoVardasPavardė;

            txtMokytojoId.Text = mokytojoId.ToString();
            txtMokytojoVardasPavardė.Text = vardasPavardė;
        }

        private void IšvalykMokytojoLaukus()
        {
            txtMokytojoId.Clear();
            txtMokytojoVardasPavardė.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            IšvalykMokytojoLaukus();
        }

        private void txtMokytojoVardasPavardė_TextChanged(object sender, EventArgs e)
        {
            var textBox = (TextBox)sender;
            if (textBox.Text.Length == 0)
            {
                IšvalykMokytojoLaukus();
            }
        }

        private void btnĮkelkFailą_Click(object sender, EventArgs e)
        {
            using (var openFileDialog = new OpenFileDialog())
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    var filePath = openFileDialog.FileName;
                    txtUžduotiesNuoroda.Text = filePath;
                }
            }
        }

        private void btnSaugoti_Click(object sender, EventArgs e)
        {
            var pateikimoData = new DateTime(datetimePateikimoData.Value.Year, datetimePateikimoData.Value.Month, datetimePateikimoData.Value.Day, datetimePateikimoLaikas.Value.Hour, datetimePateikimoLaikas.Value.Minute, 0);

            var atlikimoData = new DateTime(datetimeAtlikimoData.Value.Year, datetimeAtlikimoData.Value.Month, datetimeAtlikimoData.Value.Day, datetimeAtlikimoLaikas.Value.Hour, datetimeAtlikimoLaikas.Value.Minute, 0);

            if (IsFormValid())
            {
                
            }
        }

        private bool IsFormValid()
        {
            if (!int.TryParse(txtMokytojoId.Text, out int result))
            {
                MessageBox.Show("Klaidingai įvestas mokytojas.");
                return false;
            }

            if (!File.Exists(txtUžduotiesNuoroda.Text))
            {
                MessageBox.Show("Failas neegzistuoja.");
                return false;
            }
            return true;
        }

        private void btnAtidarykStudentųSąrašą_Click(object sender, EventArgs e)
        {
            StudentList studentList = new StudentList();
            studentList.FormClosed += StudentList_FormClosed;
            studentList.ShowDialog();
        }

        private void StudentList_FormClosed(object sender, FormClosedEventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
