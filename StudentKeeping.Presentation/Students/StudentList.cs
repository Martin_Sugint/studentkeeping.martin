﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StudentKeeping.Services;
using StudentKeeping.Dal.GridųFiltrai;
using StudentKeeping.Presentation.test;
using Extensions;
using StudentKeeping.Dal.ViewModels;

namespace StudentKeeping.Presentation.Students
{
    public partial class StudentList : Form
    {
        public List<PasirinktasStudentasUžduočiai> PasirinktiStudentai { get; set; }

        private readonly StudentsService _service;
        public StudentList()
        {
            InitializeComponent();
            _service = new StudentsService();
            UžpildykGridą(new StudentųGridFiltas());
        }

        private void UžpildykGridą(StudentųGridFiltas gridFiltras)
        {
            dataGridView1.Rows.Clear();
            var duomenys = _service.GaukStudentus(gridFiltras);
            foreach (DataRow studentas in duomenys.Rows)
            {
                var index = dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells["Id"].Value = studentas["Id"];
                dataGridView1.Rows[index].Cells["Vardas"].Value = studentas["Vardas"];
                dataGridView1.Rows[index].Cells["Pavardė"].Value = studentas["Pavarde"];
                dataGridView1.Rows[index].Cells["Adresas"].Value = studentas["Adresas"];
                dataGridView1.Rows[index].Cells["TelNr"].Value = studentas["TelNr"];
                dataGridView1.Rows[index].Cells["IsDeletedHide"].Value = studentas["IsDeleted"];
                dataGridView1.Rows[index].Cells["Klasė"].Value = studentas["Pavadinimas"];
                dataGridView1.Rows[index].Cells["KlasėsId"].Value = studentas["KlasesId"];
                dataGridView1.Rows[index].Cells["StudentoNaudotojoId"].Value = studentas["NaudotojoId"];

                if (bool.Parse(studentas["IsDeleted"].ToString()))
                {
                    dataGridView1.Rows[index].DefaultCellStyle.BackColor = Color.Red;
                }
                
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var columnName = ((DataGridView)sender).Columns[e.ColumnIndex].Name;
            var arIštrintas = (bool)dataGridView1.Rows[e.RowIndex].Cells["IsDeletedHide"].Value;

            if (columnName == "PasirinktasStudentas")
            {
                var chkbox = (DataGridViewCheckBoxCell)dataGridView1.Rows[e.RowIndex].Cells[0];
                

                if ((chkbox.Value.IsNull() || chkbox.Value == chkbox.FalseValue) && !arIštrintas)
                {
                    chkbox.Value = chkbox.TrueValue;
                } else
                {
                    chkbox.Value = chkbox.FalseValue;
                }
            }

            if (columnName == "Atnaujinti")
            {
                if (!arIštrintas)
                {
                    var id = (int)dataGridView1.Rows[e.RowIndex].Cells["Id"].Value;
                    SaveStudent saveStudent = new SaveStudent(id);
                    saveStudent.FormClosed += SaveStudent_FormClosed;
                    saveStudent.ShowDialog();
                }
            }

            if (columnName == "Ištrinti")
            {
                //Istrinti
                if (arIštrintas)
                {
                    MessageBox.Show("Šis studentas jau ištrintas");
                } else
                {
                    if (MessageBox.Show("Ar tikrai norite ištrinti šį studentą?", "Ištrinti studentą", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        var id = (int)dataGridView1.Rows[e.RowIndex].Cells["Id"].Value;
                        _service.IšrinkStudentą(id);
                        UžpildykGridą(GaukStudentųGridFiltrą());
                    }
                }
            }
        }

        private void SaveStudent_FormClosed(object sender, FormClosedEventArgs e)
        {
            UžpildykGridą(GaukStudentųGridFiltrą());
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            UžpildykGridą(GaukStudentųGridFiltrą());
        }

        private StudentųGridFiltas GaukStudentųGridFiltrą()
        {
            return new StudentųGridFiltas
            {
                Vardas = txtVardas.Text,
                Pavardė = txtPavardė.Text,
                TelNr = txtTelNr.Text,
                Adresas = txtAdresas.Text,
                ArRodytiIrIštrintus = checkRodytiVisus.Checked,
                Klasė = txtKlasė.Text
            };
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void chkboxPažymėtiVisus_CheckedChanged(object sender, EventArgs e)
        {
            var chkbox = (CheckBox)sender;
            if (chkbox.Checked == true)
            {
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if ((bool)row.Cells["IsDeletedHide"].Value == false)
                    {
                        row.Cells[0].Value = true;
                    }
                }
            } else
            {
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if ((bool)row.Cells["IsDeletedHide"].Value == false)
                    {
                        row.Cells[0].Value = false;
                    }
                }
            }
        }

        private void btnPasirinkti_Click(object sender, EventArgs e)
        {
            PasirinktiStudentai = new List<PasirinktasStudentasUžduočiai>();

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if ((bool)row.Cells["IsDeletedHide"].Value == false && (bool?)row.Cells[0].Value == true)
                {
                    var naudotojoId = (int?)row.Cells["StudentoNaudotojoId"].Value;
                    var vardas = (string)row.Cells["Vardas"].Value;
                    var pavardė = row.Cells["Pavardė"].Value;
                    var klasė = row.Cells["Klasė"].Value.ToString();

                    var pasirinktasStudentas = new PasirinktasStudentasUžduočiai
                    {
                        NaudotojoId = naudotojoId,
                        VardasPavardė = vardas + " " + pavardė,
                        Klasė = klasė
                    };

                    PasirinktiStudentai.Add(pasirinktasStudentas);

                }
            }

            this.Close();
        }
    }
}
