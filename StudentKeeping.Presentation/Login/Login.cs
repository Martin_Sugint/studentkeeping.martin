﻿using StudentKeeping.Dal.ViewModels;
using StudentKeeping.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Extensions;

namespace StudentKeeping.Presentation.Login
{
    public partial class Login : Form
    {
        private readonly LoginService _service;

        public Login()
        {
            _service = new LoginService();
            InitializeComponent();
            txtLogin.Text = "anapav";
            txtPassword.Text = "slaptazodis2";
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            Prisijungimas prisijungimas = new Prisijungimas
            {
                NaudotojoVardas = txtLogin.Text,
                Slaptažodis = txtPassword.Text
            };

            prisijungimas = _service.Prisijungti(prisijungimas);

            PakeiskStatusoLabel(prisijungimas.PrisijungimoStatusas);

            if (prisijungimas.PrisijungimoStatusas == PrisijungimoStatusas.Prisijugta)
            {
                Session.SukurkSesija(prisijungimas.Sesija.NaudotojoId, prisijungimas.Sesija.NaudotojoVardas, prisijungimas.Sesija.Vardas, prisijungimas.Sesija.Pavarde, prisijungimas.Sesija.Role);

                this.Hide();
                Užduotys.UžduotiesSaugojimas forma = new Užduotys.UžduotiesSaugojimas();
                forma.Show();
            }
        }

        private void PakeiskStatusoLabel(PrisijungimoStatusas? prisijungimoStatusas)
        {
            if (prisijungimoStatusas.IsNull() || prisijungimoStatusas.Value == PrisijungimoStatusas.Prisijugta)
            {
                lblStatusas.Text = "";
            }

            if (prisijungimoStatusas.HasValue && prisijungimoStatusas.Value == PrisijungimoStatusas.NaudotojasNerastas)
            {
                lblStatusas.Text = "Naudotojas nerastas";
                lblStatusas.ForeColor = Color.Red;
            }

            if (prisijungimoStatusas.HasValue && prisijungimoStatusas.Value == PrisijungimoStatusas.NeteisingasSlaptažodis)
            {
                lblStatusas.Text = "Neteisingas slaptažodis";
                lblStatusas.ForeColor = Color.Red;
            }
        }
    }
}
