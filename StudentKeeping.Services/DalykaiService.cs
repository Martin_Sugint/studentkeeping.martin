﻿using StudentKeeping.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentKeeping.Dal.Models;
using MySql.Data.MySqlClient;
using System.Data;

namespace StudentKeeping.Services
{
    public class DalykaiService : DBConnection
    {
        public DalykaiService() { }

        public List<Dalykas> GaukDalykus()
        {
            var cmdText = "SELECT * FROM Dalykai";

            var cmd = new MySqlCommand(cmdText, Connection);

            DataTable dalykai = new DataTable();
            MySqlDataAdapter dalykaiAdapter = new MySqlDataAdapter(cmd);

            dalykaiAdapter.Fill(dalykai);

            return dalykai.AsEnumerable().Select(x => new Dalykas
            {
                Id = x.Field<int>("Id"),
                Pavadinimas = x.Field<string>("Pavadinimas")
            }).ToList();
        }
    }
}
