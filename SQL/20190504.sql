CREATE DATABASE  IF NOT EXISTS `studentkeeping` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `studentkeeping`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: studentkeeping
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dalykai`
--

DROP TABLE IF EXISTS `dalykai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dalykai` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Pavadinimas` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dalykai`
--

LOCK TABLES `dalykai` WRITE;
/*!40000 ALTER TABLE `dalykai` DISABLE KEYS */;
INSERT INTO `dalykai` VALUES (1,'Matematika'),(2,'Dailė'),(3,'Lietuvių k.'),(4,'Anglų k.'),(5,'Chemija'),(6,'Fizika');
/*!40000 ALTER TABLE `dalykai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mokytojai`
--

DROP TABLE IF EXISTS `mokytojai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mokytojai` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Vardas` varchar(250) DEFAULT NULL,
  `Pavarde` varchar(250) DEFAULT NULL,
  `DalykoId` int(11) DEFAULT NULL,
  `NaudotojoId` int(11) DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `DalykoId` (`DalykoId`),
  KEY `NaudotojoId` (`NaudotojoId`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mokytojai`
--

LOCK TABLES `mokytojai` WRITE;
/*!40000 ALTER TABLE `mokytojai` DISABLE KEYS */;
INSERT INTO `mokytojai` VALUES (1,'Marytė','Pauliukaitienė',1,NULL,0),(2,'Augustas','Martinaitis',1,NULL,0),(3,'Solveiga','Auštytė',2,NULL,0),(4,'Tadas','Fisavičius',3,NULL,0),(5,'Mantas','Aukštavičius',2,NULL,0),(6,'Ieva','Odošenko',4,NULL,0),(7,'Karolina','Petrovič',6,NULL,0),(8,'Modesta','Puodžiūkė',3,NULL,0),(9,'Ignas','Gaidys',5,NULL,0);
/*!40000 ALTER TABLE `mokytojai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `naudotojai`
--

DROP TABLE IF EXISTS `naudotojai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `naudotojai` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PrisijungimoVardas` varchar(20) DEFAULT NULL,
  `Slaptazodis` varchar(50) DEFAULT NULL,
  `Vardas` varchar(80) DEFAULT NULL,
  `Pavarde` varchar(80) DEFAULT NULL,
  `Role` int(11) DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `Role` (`Role`),
  CONSTRAINT `naudotojai_ibfk_1` FOREIGN KEY (`Role`) REFERENCES `roles` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `naudotojai`
--

LOCK TABLES `naudotojai` WRITE;
/*!40000 ALTER TABLE `naudotojai` DISABLE KEYS */;
INSERT INTO `naudotojai` VALUES (1,'arūpav','slaptazodis1','Arūnas','Pavarde',2,0),(2,'anapav','slaptazodis2','Anastasija','Pavarde',2,0),(3,'aropav','slaptazodis3','Aronas','Pavarde3',2,0);
/*!40000 ALTER TABLE `naudotojai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Pavadinimas` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Mokytojas'),(2,'Moksleivis');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studentai`
--

DROP TABLE IF EXISTS `studentai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studentai` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Vardas` varchar(100) DEFAULT NULL,
  `Pavarde` varchar(200) DEFAULT NULL,
  `TelNr` varchar(20) DEFAULT NULL,
  `Adresas` varchar(200) DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `NaudotojoId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `NaudotojoId` (`NaudotojoId`),
  CONSTRAINT `studentai_ibfk_1` FOREIGN KEY (`NaudotojoId`) REFERENCES `naudotojai` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studentai`
--

LOCK TABLES `studentai` WRITE;
/*!40000 ALTER TABLE `studentai` DISABLE KEYS */;
INSERT INTO `studentai` VALUES (1,'Arūnas','Pavarde','','Aguonu g. 1',1,1),(2,'Anastasija','Pavarde','869545159','',1,2),(3,'Aronas','Pavarde3','869545159','Geliu g. 23',1,3),(4,'Saulius','Pavarde1',NULL,'Aguonu g. 1',1,NULL),(5,'Mantas','Pavarde2','869545159',NULL,1,NULL),(6,'Tadas','Pavarde3','869545159','Geliu g. 23',1,NULL),(7,'Tadeuš','Pavarde1',NULL,'Aguonu g. 1',1,NULL),(8,'Martynas','Pavarde2','869545159',NULL,0,NULL),(9,'Stanislavas','Pavarde3','869545159','Geliu g. 23',1,NULL),(10,'Viktorija','Pavarde1',NULL,'Aguonu g. 1',1,NULL),(11,'Aušra','Pavarde2','869545159',NULL,0,NULL),(12,'Saulė','Pavarde3','869545159','Geliu g. 23',1,NULL);
/*!40000 ALTER TABLE `studentai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uzduotys`
--

DROP TABLE IF EXISTS `uzduotys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uzduotys` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `DalykoId` int(11) DEFAULT NULL,
  `MokytojoId` int(11) DEFAULT NULL,
  `MokytojoVardasPavarde` varchar(255) DEFAULT NULL,
  `Pavadinimas` varchar(250) DEFAULT NULL,
  `Tema` varchar(250) DEFAULT NULL,
  `UžduotiesNuoroda` varchar(1000) DEFAULT NULL,
  `IkelimoData` datetime DEFAULT NULL,
  `PateikimoData` datetime DEFAULT NULL,
  `IkiKadaAtlikti` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uzduotys`
--

LOCK TABLES `uzduotys` WRITE;
/*!40000 ALTER TABLE `uzduotys` DISABLE KEYS */;
/*!40000 ALTER TABLE `uzduotys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'studentkeeping'
--

--
-- Dumping routines for database 'studentkeeping'
--
/*!50003 DROP PROCEDURE IF EXISTS `Prisijungti` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Prisijungti`(
	IN InNaudotojoVardas VARCHAR(20),
    IN InSlaptazodis VARCHAR(50),
    OUT PrisijungimoStatusas int,
    OUT OutVardas varchar(80),
    OUT OutPavarde varchar(80),
    OUT OutRole int
)
asd:BEGIN
	
	IF((SELECT COUNT(1) FROM naudotojai 
		WHERE PrisijungimoVardas = InNaudotojoVardas) = 0) THEN
        SET PrisijungimoStatusas = 1;
        LEAVE asd;
	END IF;
    
    IF ((SELECT COUNT(1) FROM naudotojai 
		WHERE PrisijungimoVardas = InNaudotojoVardas 
        and Slaptazodis = InSlaptazodis) = 0) THEN
			SET PrisijungimoStatusas = 2;
			LEAVE asd;
		ELSE 
			SET PrisijungimoStatusas = 3;
	END IF;
    
    IF(PrisijungimoStatusas = 3) THEN
		SELECT Vardas, Pavarde, role
		INTO OutVardas, OutPavarde, OutRole 
        FROM naudotojai
        WHERE PrisijungimoVardas = InNaudotojoVardas;
    END IF;
    
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-04 12:54:23
